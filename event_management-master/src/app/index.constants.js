/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('eventManageApp')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
