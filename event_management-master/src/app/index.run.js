(function() {
  'use strict';

  angular
    .module('eventManageApp')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
