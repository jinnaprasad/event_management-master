(function() {
  'use strict';

  angular
    .module('eventManageApp')
    .controller('MainController', MainController);

  function MainController(toastr) {
    var vm = this;

    vm.showToastr = showToastr;

    function showToastr() {
      toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
      vm.classAnimation = '';
    }
  }
})();
