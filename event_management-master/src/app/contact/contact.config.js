'use strict';
angular.module('eventManageApp').config(function ($stateProvider) {
    $stateProvider.state('contact', {
        url: '/contact',
        templateUrl: 'app/contact/contact.html',
        controller: 'ContactController',
        page: {
            title: 'Contact',
            description: ''
        }
    });
});
