'use strict';
angular.module('eventManageApp').config(function ($stateProvider) {
    $stateProvider.state('join', {
        url: '/join',
        templateUrl: 'app/join/join.html',
        controller: 'joinController',
        page: {
            title: 'Join',
            description: ''
        }
    });
});
