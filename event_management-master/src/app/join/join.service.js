'use strict'

angular.module('eventManageApp')
    .factory('JoinService',['$http', function($http){
        var joinServiceFactory= {};
        var urlBase = '/api/joins';

        joinServiceFactory.getEmptyVolunteer = function(){
            return {
                id:'',
                firstName:'',
                lastName:'',
                email:'',
                gender:'',
                regularRunner:false,
                racer:false,
                previousParticipant:false,
                about:''
            }
        };

        joinServiceFactory.saveVolunteer = function(volunteer){
            console.log(volunteer);
            return volunteer;
          /*  if(join.id === ''){
                return $http.post(urlBase, volunteer).then(function(response){
                    return response;
                },function(response){
                    return response;
                });
            }else{
                return $http.put(urlBase, volunteer).then(function(response){
                    return response;
                },function(response){
                    return response;
                });
            }*/
        };

        return joinServiceFactory;
    }]);
