'use strict';

angular.module('eventManageApp').controller('joinController', [
    '$scope','JoinService', function ($scope, JoinService) {

        $scope.volunteer = JoinService.getEmptyVolunteer();

        $scope.saveVolunteer = function(){
            JoinService.saveVolunteer($scope.volunteer);
        };      
    }
]);